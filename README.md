# ts-node-starter

Base environment for run TypeScript of use ts-node.

## Creation Process of this Repository.

```
  # create package.json
$ npm init

  # install TypeScript
$ npm install --save-dev typescript

  # install ts-node
$ npm install --save-dev ts-node

  # create tsconfig.json
$ npx tsc --init

  # create src/main.ts
$ mkdir src && touch src/main.ts
```

## Run Code of main.ts

```
$ npm run main
```
